import STYLES from './map-style';

export default () => {

  global.initMap = function(targetElement, mapCenter, data, styleData) {

      'use strict';

      var locations = data;

      var centerPosition = new google.maps.LatLng(mapCenter[0],mapCenter[1]);
      var defaultZoom = 14;
      var delay = {
          animate: true,
          duration: 5000
      };

      var map = new google.maps.Map(targetElement, {
          zoom: 14,
          center: centerPosition,
          scrollwheel: true,
          navigationControl: true,
          mapTypeControl: true,
          scaleControl: true,
          draggable: true,
          mapTypeId: google.maps.MapTypeId.ROADMAP,
          styles: STYLES
      });
      var iconBase = "images/marker.png";


      var marker, i;
      var markers = [];

      for (i = 0; i < locations.length; i++) {
          marker = new google.maps.Marker({
              position: new google.maps.LatLng(locations[i][1], locations[i][2]),
              map: map,
              title: locations[i][0],
              icon: {
                  url: iconBase,
                  scaledSize:  new google.maps.Size(60, 90)
              }
          });

          markers.push(marker);

      }
  }
}
