require('jquery-mousewheel');
require('malihu-custom-scrollbar-plugin');

export default () => {
  if ($(window).width() < 767) {
    $(".cooperation__list-box").mCustomScrollbar({
      axis: "x"
    });
  }
}
