require('jquery.maskedinput/src/jquery.maskedinput.js');
require("jquery-countto");

export default () => {

  $('.viewport').viewportChecker({
    classToAdd: 'visible',
    offset: '20%',
    removeClassAfterAnimation: false
  });

  if ($(".cooperation__list").length > 0) {
    var section1 = $(".cooperation__list");
    var cc1 = 1;
    $(window).scroll(function() {
      var targetPos1 = section1.offset().top;
      var winHeight1 = $(window).height();
      var scrollToElem1 = targetPos1 - winHeight1;
      var winScrollTop1 = $(this).scrollTop();

      if (winScrollTop1 > scrollToElem1) {
        if (cc1 < 2) {
          cc1 = cc1 + 2;
          $('.timer').countTo({
            refreshInterval: 50
          });
        }
      }
    });
  }

  // __mob_menu
  $('.menu-icon').on("click", function() {
    $(this).toggleClass("active");
    $('.header__mobile').toggleClass('active');
  });
  $('.header').on("click", ".header__mobile-overlay", function() {
    $('.header__mobile').removeClass('active');
    $('.menu-icon').removeClass('active');
  });

  $('.callback').on('click', function() {
    $('.popup-callback').addClass('active');
    $('body').addClass('active');
  });
  $('.feedback').on('click', function() {
    $('.popup-feedback').addClass('active');
    $('body').addClass('active');
  });
  $('.overlay, .popup__close').on('click', function() {
    $('.popup').removeClass('active');
    $('body').removeClass('active');
  });

  jQuery(function($){
      $("input[type=tel]").mask("+38(099)9999999");
      if ($(window).width() < 767) {
         $("input[type=tel]").mask("+380999999999");
      } else {
        $("input[type=tel]").mask("+38(099)9999999");
      }
    });

  $('.top').click(function() {
    $('body,html').animate({
      scrollTop: 0
    }, 500);
  });

  $(window).scroll(function() {
    if ($(this).scrollTop() >= 80) {
      $('.header').addClass('active');
    } else {
      $('.header').removeClass('active');
    }
  });

  $('.scroll-btn, .nav__list a').click(function() {
    var scroll_el = $(this).attr('href');
    if ($(scroll_el).length != 0) {
      if ($(window).width() < 767) {
        $('html, body').animate({
          scrollTop: $(scroll_el).offset().top - 53
        }, 500);
      } else {
        $('html, body').animate({
          scrollTop: $(scroll_el).offset().top - 80
        }, 500);
      }
    }
    return false;
  });

  if ($(window).width() < 1200) {
    $('.nav__list a').on('click', function() {
      $('.header__mobile, .menu-icon').removeClass('active');
    })
  }
}
