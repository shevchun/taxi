export default () => {

  $('.reviews__slider').slick({
    infinite: true,
    slidesToShow: 2,
    slidesToScroll: 1,
    accessibility: false,
    arrows: true,
    dots: false,
    prevArrow: '<span class="slick-prev"><svg width="6" height="10"><use xlink:href="/images/sprite.svg#left"/></svg></span>',
    nextArrow: '<span class="slick-next"><svg width="6" height="10"><use xlink:href="/images/sprite.svg#left"/></svg></span>',
    responsive: [
    {
      breakpoint: 767,
      settings: {
        slidesToShow: 1,
      }
    }
  ]
  });

}
