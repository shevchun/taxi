// import "./markup-menu";

import $ from 'jquery';
global.$ = global.jQuery = $;
import custom from './components/custom';
import scroll from './components/scroll';
import sliders from './components/sliders';
import slick from 'slick-carousel';
import svg4everybody from 'svg4everybody';
import map from './components/map';
require("jQuery-viewport-checker/dist/jquery.viewportchecker.min.js");

document.addEventListener("DOMContentLoaded", () => {
  custom();
  svg4everybody();
  scroll();
  sliders();
  map();
  $(".scroll").mCustomScrollbar();
});

$(window).on('load resize', () => {
  if ($(window).width() < 1200) {
    $('.header__mobile-box').append($('.nav'));
    $('.header__mobile-box').append($('.header__tel-box'));
    $('.header__mobile-box').append($('.header__mail'));
    $('.header__mobile-box').append($('.language'));
  } else {
    $('.nav').insertAfter($('.header__logo'));
    $('.header__tel-box').insertBefore($('.callback'));
    $('.header__mail').insertBefore($('.callback'));
    $('.language').insertBefore($('.callback'));
  }
});

$(window).on('load resize', () => {
  if ($(window).width() < 1200) {
    $('.preloader').addClass('active');
  }
});

$(window).on('load', () => {
  $('.top-section__caption').addClass('active');
  $('.top-section__btns').addClass('active');
  setTimeout(function() {
    $('.header__mail').addClass('load');
    $('.language').addClass('load');
  }, 500);
});
