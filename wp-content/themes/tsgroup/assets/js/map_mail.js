jQuery(document).ready(function() {
    var coords_map = mVars.coords;
    var coords_lng = parseFloat(coords_map[0]);
    var coords_lat = parseFloat(coords_map[1]);
    initMap(
        document.getElementById('map'), [coords_lng, coords_lat], [
            [, coords_lng, coords_lat]
        ]
    );
    // cf7_success
    var wpcf7Elm = document.querySelector( '.wpcf7' );
    if(wpcf7Elm){
        wpcf7Elm.addEventListener( 'wpcf7mailsent', function( event ) {
            jQuery('.popup-callback').removeClass('active');
            jQuery('.popup-feedback').removeClass('active');
            jQuery('body').removeClass('active');
            jQuery('.popup-thankYou').addClass('active');
            setTimeout(function(){
                jQuery('.popup-thankYou').removeClass('active');
            }, 5000);
        }, false );
    }
    document.addEventListener( 'wpcf7mailsent', function( event ) {
        jQuery('.popup-callback').removeClass('active');
        jQuery('.popup-feedback').removeClass('active');
        jQuery('body').removeClass('active');
        jQuery('.popup-thankYou').addClass('active');
        setTimeout(function(){
            jQuery('.popup-thankYou').removeClass('active');
        }, 5000);
    }, false );
});