<?php
global $q_config;
$instagram = get_field('instagram','option')?get_field('instagram','option') : '';
$facebook = get_field('facebook','option')?get_field('facebook','option') : '';
$twitter = get_field('twitter','option')?get_field('twitter','option') : '';

$form_callback_title = get_field('form_callback_title','option')?get_field('form_callback_title','option') : '';
$form_callback2_title = get_field('form_callback2_title','option')?get_field('form_callback2_title','option') : '';

$thanks_title = get_field('thanks_title','option')?get_field('thanks_title','option') : '';
$thanks_text = get_field('thanks_text','option')?get_field('thanks_text','option') : '';
$lemon_url = 'https://art-lemon.com/'
?>
<footer class="footer">
    <div class="container">
        <div class="footer__box">
            <?php if($q_config['language'] == 'ua'):
	            $lemon_url = 'https://art-lemon.com/ua';
            elseif($q_config['language'] == 'en'):
	            $lemon_url = 'https://art-lemon.com/en';
            else:
	            $lemon_url = 'https://art-lemon.com/';
            endif;
            ?>
            <a href="<?php echo $lemon_url;?>" class="dev">
                <div class="dev__text"><?php echo _e('Разработка сайта','tsgroup'); ?></div>
                <div class="dev__icons">
                    <div class="dev__icon">
                        <div class="svg"><svg width="72" height="11"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?php echo TX_URL_THEME; ?>/images/sprite.svg#lemon"></use></svg></div>
                    </div>
                    <div class="dev__hover">
                        <div class="svg"><svg width="72" height="11"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?php echo TX_URL_THEME; ?>/images/sprite.svg#lemon"></use></svg></div>
                    </div>
                </div>
            </a>
            <?php if(!empty($instagram) || !empty($facebook) || !empty($twitter)):?>
            <ul class="social">
                <?php if(!empty($instagram)):?>
                     <li class="social__item">
                         <a href="<?php echo $instagram; ?>" class="social__link" target="_blank" rel="nofollow">
                             <?php _e('instagram','tsgroup');?>
                         </a>
                     </li>
                <?php endif; ?>
	            <?php if(!empty($facebook)):?>
                    <li class="social__item">
                        <a href="<?php echo $facebook; ?>" class="social__link" target="_blank" rel="nofollow">
                            <?php _e('facebook','tsgroup');?>
                        </a>
                    </li>
	            <?php endif; ?>
	            <?php if(!empty($twitter)):?>
                    <li class="social__item">
                        <a href="<?php echo $twitter; ?>" class="social__link" target="_blank" rel="nofollow">
                            <?php _e('twitter','tsgroup');?>
                        </a>
                    </li>
	            <?php endif; ?>
            </ul>
            <?php endif; ?>
            <div class="top">
                <div class="svg"><svg width="14" height="8"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?php echo TX_URL_THEME; ?>/images/sprite.svg#down"></use></svg></div>
            </div>
        </div>
    </div>
</footer>
<?php wp_footer(); ?>

<div class="popup popup-callback">
    <div class="overlay"></div>
    <div class="popup__box">
        <div class="popup__close">
            <div class="svg"><svg width="10" height="10"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?php echo TX_URL_THEME; ?>/images/sprite.svg#close"></use></svg></div>
        </div>
        <div class="popup__title"><?php echo $form_callback_title; ?></div>
	    <?php
	    if($q_config['language'] == 'ru'):
		    echo do_shortcode('[contact-form-7 title="Обратный Звонок RU" html_class="popup__form"]');
	    elseif($q_config['language'] == 'ua'):
		    echo do_shortcode('[contact-form-7 title="Обратный Звонок UA" html_class="popup__form"]');
	    else:
		    echo do_shortcode('[contact-form-7 title="Обратный Звонок EN" html_class="popup__form"]');
	    endif;
        ?>
    </div>
</div>
<div class="popup popup-feedback">
    <div class="overlay"></div>
    <div class="popup__box">
        <div class="popup__close">
            <div class="svg"><svg width="10" height="10"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?php echo TX_URL_THEME; ?>/images/sprite.svg#close"></use></svg></div>
        </div>
        <div class="popup__title"><?php echo $form_callback2_title; ?></div>
	    <?php
	    if($q_config['language'] == 'ru'):
		    echo do_shortcode('[contact-form-7 title="Обратная связь RU" html_class="popup__form"]');
        elseif($q_config['language'] == 'ua'):
		    echo do_shortcode('[contact-form-7 title="Обратная связь UA" html_class="popup__form"]');
	    else:
		    echo do_shortcode('[contact-form-7 title="Обратная связь EN" html_class="popup__form"]');
	    endif;
	    ?>
    </div>
</div>
<?php $time_from = get_field('time_form_start','option')?get_field('time_form_start','option') : '';
$time_end = get_field('time_form_end','option')?get_field('time_form_end','option') : '';
$time_end = (int)$time_end -1;
$time_from = (int)$time_from;
$current_time = date('H',time()) + 2;
$tail = __('в течении 15 минут','tsgroup');
if($current_time >= $time_from && $current_time <= $time_end ){
	$tail = get_field('tail_soon','option')?get_field('tail_soon','option') : '';
} else {
	$tail = get_field('tail_late','option')?get_field('tail_late','option') : '';
}

?>
<div class="popup popup-thankYou">
    <div class="overlay"></div>
    <div class="popup__box">
        <div class="popup__close">
            <div class="svg">
                <svg width="10" height="10">
                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?php echo TX_URL_THEME; ?>/images/sprite.svg#close"></use>
                </svg>
            </div>
        </div>
        <div class="popup__title"><?php echo $thanks_title; ?></div>
        <div class="popup__description"><?php echo $thanks_text; ?> <?php echo $tail; ?></div>
    </div>
</div>
<?php wp_footer(); ?>
</body>

</html>