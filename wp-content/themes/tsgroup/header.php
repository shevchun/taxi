<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="UTF-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="description" content="<?php echo get_bloginfo('description'); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php
	    $page_title = '';
        $front_title = get_field('front_title','option')?get_field('front_title','option') : '';
	    if(is_404()) {
		    $page_title = get_bloginfo('name').' | Error Page';
        } elseif(is_category()) {
            $page_title = single_cat_title(get_bloginfo('name').' | ');
	    } elseif(is_front_page()) {
		    $page_title = get_bloginfo('name').' | '.($front_title);
	    } else {
		    $page_title = get_bloginfo('name').' | '.$front_title;
	    }

	    echo $page_title;
	    ?></title>
    <link rel="icon" type="image/png" sizes="192x192" href="<?php echo TX_URL_THEME; ?>/images/192x192.png">
    <link rel="icon" type="image/png" sizes="180x180" href="<?php echo TX_URL_THEME; ?>/images/64x64.png">
    <link rel="icon" type="image/png" sizes="32x32" href="<?php echo TX_URL_THEME; ?>/images/48x48.png">
    <link rel="icon" type="image/png" sizes="16x16" href="<?php echo TX_URL_THEME; ?>/images/32x32.png">
    <?php wp_head();?>
</head>


<body>
<?php
$logo_title = get_field('logo_title','option')?get_field('logo_title','option') : '';

$vacancy_title = get_field('vacancy_title','option')?get_field('vacancy_title','option') : '';
$about_title = get_field('about_title','option')?get_field('about_title','option') : '';
$cooperation_title = get_field('cooperation_title','option')?get_field('cooperation_title','option') : '';
$demands_title = get_field('demands_title','option')?get_field('demands_title','option') : '';
$reviews_title = get_field('reviews_title','option')?get_field('reviews_title','option') : '';
$contacts_title = get_field('contacts_title','option')?get_field('contacts_title','option') : '';

$hotline_title = get_field('hotline_title','option')?get_field('hotline_title','option') : '';
$header_phone = get_field('header_phone','option')?get_field('header_phone','option') : '';

$callback_title = get_field('callback_title','option')?get_field('callback_title','option') : '';
$header_mail = get_field('header_mail','option')?get_field('header_mail','option') : '';
?>
<div class="wrapper">
    <div class="preloader">
        <div class="preloader__load">
            <hr/><hr/><hr/><hr/>
        </div>
    </div>
    <div class="menu-icon">
        <span id="bar-1" class="line"></span>
        <span id="bar-2" class="line"></span>
        <span id="bar-3" class="line"></span>
    </div>
    <header class="header">
        <div class="header__logo">
            <a href="<?php echo home_url();?>" class="header__logo-link"><?php echo $logo_title?></a>
        </div>
        <nav class="nav">
            <ul class="nav__list">
                <?php if(!empty($vacancy_title)):?>
                    <li><a href="#vacancy"><?php echo $vacancy_title; ?></a></li>
                <?php endif; ?>
	            <?php if(!empty($about_title)):?>
                    <li><a href="#about"><?php echo $about_title; ?></a></li>
	            <?php endif; ?>
	            <?php if(!empty($cooperation_title)):?>
                    <li><a href="#cooperation"><?php echo $cooperation_title; ?></a></li>
	            <?php endif; ?>
	            <?php if(!empty($demands_title)):?>
                    <li><a href="#demands"><?php echo $demands_title; ?></a></li>
	            <?php endif; ?>
	            <?php if(!empty($reviews_title)):?>
                    <li><a href="#reviews"><?php echo $reviews_title; ?></a></li>
	            <?php endif; ?>
	            <?php if(!empty($contacts_title)):?>
                    <li><a href="#contacts"><?php echo $contacts_title; ?></a></li>
	            <?php endif; ?>
            </ul>
        </nav>
        <?php if(!empty($header_phone) || !empty($hotline_title)):?>
            <div class="header__tel-box">
                <div class="header__tel-text"><?php echo $hotline_title; ?></div>
                <a href="tel:<?php echo str_replace(array(' ','(',')','-'),'',$header_phone); ?>"
                   class="header__tel">
                    <?php echo $header_phone; ?>
                </a>
            </div>
        <?php endif; ?>
	    <?php if(!empty($callback_title) || !empty($header_mail)):?>
            <div class="callback btn"><span><?php echo $callback_title; ?></span></div>
            <a href="mailto:<?php echo $header_mail; ?>" class="header__mail">
                <?php echo $header_mail; ?>
            </a>
	    <?php endif; ?>
        <ul class="language">
	        <?php
	        global $q_config;
	        foreach(qtranxf_getSortedLanguages() as $language):
		        if($language != $q_config['language'] && $language != 'en'):?>
                    <li class="language__item">
                    <a class="language__link" href="<?php $url = '';
			        echo qtranxf_convertURL($url, $language, false, true); ?>">
				        <?php echo $language; ?>
                    </a>
			        <?php
                elseif($language != $q_config['language'] && $language == 'en'):?>
                    <li class="language__item hidden-lang">
                    <a class="language__link" href="<?php $url = '';
			        echo qtranxf_convertURL($url, $language, false, true); ?>">
				        <?php echo $language; ?>
                    </a>
		        <? else:?>
                    <li class="language__item active">
                    <a href="<?php $url = '';
			        echo qtranxf_convertURL($url, $language, false, false); ?>" class="language__link"><?php echo $language; ?></a>
                    </li>
			        <?php
		        endif;
	        endforeach; ?>
        </ul>
        <div class="header__mobile">
            <div class="header__mobile-overlay"></div>
            <div class="header__mobile-box"></div>
        </div>
    </header>