<?php
require_once 'inc/autoloader.php';
define('TX_URL_THEME', get_template_directory_uri().'/assets');

function tx_custom_scripts() {
	wp_enqueue_style( 'atrium-theme', get_stylesheet_uri(),array(),null );
	wp_enqueue_style( 'atrium-main-theme', get_template_directory_uri().'/assets/css/index.css',array(),null );
	wp_enqueue_style( 'google-fonts', 'https://fonts.googleapis.com/css?family=Montserrat:800i%7CRoboto:700,900i%7CUbuntu:300,400,500,700',array(),null );
	wp_enqueue_script( 'app', get_template_directory_uri(). '/assets/js/app.js' , array(), null, true );
	wp_enqueue_script( 'map-script', 'https://maps.googleapis.com/maps/api/js?key=AIzaSyAe0e7RzQTppvzTEUNRuNq5PqhpSOpF7yA',array(),null );
	wp_enqueue_script( 'map-mail', get_template_directory_uri(). '/assets/js/map_mail.js' , array(), null, true );
//	wp_enqueue_script( 'contact-form', get_template_directory_uri(). '/assets/js/cf7_success.js' , array(), null, true );

}

function tx_load_theme_textdomain() {
	load_theme_textdomain( 'tsgroup', get_template_directory() . '/languages' );
}

add_action( 'after_setup_theme', 'tx_load_theme_textdomain' );

add_action( 'wp_enqueue_scripts', 'tx_custom_scripts' );

add_theme_support('post-thumbnails');

//add_theme_support('widgets');

function field_get_image($field_name, $size_name, $is_sub = false){
	$field_image = '';
	if(!$is_sub){
		$field_image = get_field($field_name);
	}else{
		$field_image = get_sub_field($field_name);
	}
	$field_image = isset($field_image['sizes'][$size_name]) ? $field_image['sizes'][$size_name] : get_template_directory_uri() . '/assets/images/noimage.jpg';
	echo $field_image;
}

if( function_exists('acf_add_options_page') ) {

	acf_add_options_page('Site Settings');

}

global $q_config;
if($q_config['language'] == 'ru'):
	$username_error = 'Имя указано неверно';
	$email_error = 'Почта указана неверно';
elseif($q_config['language'] == 'ua'):
	$username_error = 'Ім\'я вказано невірно';
	$email_error = 'Пошта вказана невірно';
else:
	$username_error = 'Are you sure this is the correct name?';
	$email_error = 'Are you sure this is the correct email?';
endif;

add_filter( 'wpcf7_validate_email*', 'ta_custom_email_confirmation_validation_filter', 20, 2 );

function ta_custom_email_confirmation_validation_filter( $result, $tag ) {
	if ( 'usermail' == $tag->name ) {
		global $email_error;
		$email = isset( $_POST['usermail'] ) ? trim( $_POST['usermail'] ) : '';
		preg_match('/.+@.+\..+/i', $email, $matches, PREG_OFFSET_CAPTURE);
		if (empty($matches)) {
			$result->invalidate( $tag,$email_error);
		}
	}

	return $result;
}

add_filter( 'wpcf7_validate_text', 'ta_custom_name_confirmation_validation_filter', 20, 2 );

function ta_custom_name_confirmation_validation_filter( $result, $tag ) {
	if ( 'username' == $tag->name ) {
		global $username_error;
		$name = isset( $_POST['username'] ) ? trim( $_POST['username'] ) : '';
		preg_match('/^\D[A-Za-zА-Яа-яЁё\s]{2,}$/u', $name, $matches, PREG_OFFSET_CAPTURE);
		if(strlen($name) > 0){
			if(empty($matches)){
				$result->invalidate( $tag,$username_error);
			}
		}
	}
	return $result;
}

function registerCustomAdminCss(){
	$src = get_template_directory_uri(). '/assets/css/admin.css';
	$handle = "customAdminCss";
	wp_register_script($handle, $src);
	wp_enqueue_style($handle, $src, array(), false, false);
}
add_action('admin_head', 'registerCustomAdminCss');