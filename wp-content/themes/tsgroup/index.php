<?php

get_header();

if( have_rows('home_page_settings','option') ):

	// loop through the rows of data
	while ( have_rows('home_page_settings','option') ) : the_row();

		get_template_part('template-parts/section', get_row_layout());

	endwhile;

endif;
get_footer();