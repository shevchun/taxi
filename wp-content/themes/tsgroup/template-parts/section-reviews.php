<?php if(get_sub_field('show_reviews')):
	$reviews_title = get_sub_field('reviews_title')?get_sub_field('reviews_title'):'';
?>
<div class="reviews viewport" id="reviews">
	<div class="container-inner">
		<div class="title"><?php echo $reviews_title; ?></div>
		<?php if(have_rows('reviews')):?>
			<div class="reviews__slider">
				<?php while(have_rows('reviews')): the_row();?>
					<div class="reviews__item">
						<div class="reviews__item-title"><?php echo get_sub_field('name'); ?></div>
						<div class="reviews__item-position"><?php echo get_sub_field('position'); ?></div>
						<div class="reviews__text text-content scroll">
							<?php echo get_sub_field('description'); ?>
						</div>
					</div>
				<?php endwhile; ?>
			</div>
		<?php endif; ?>
	</div>
</div>
<?php endif; ?>