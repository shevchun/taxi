<?php
$top_image = get_sub_field('top_image')?get_sub_field('top_image'):'';
$top_title = get_sub_field('top_title')?get_sub_field('top_title'):'';
$top_want = get_sub_field('top_want')?get_sub_field('top_want'):'';
$top_cooperation = get_sub_field('top_cooperation')?get_sub_field('top_cooperation'):'';
?>
<div class="top-section">
	<div class="top-section__box" style="background-image: url(<?php echo $top_image; ?>);">
		<div class="top-section__holder">
			<div class="top-section__caption"><?php echo $top_title; ?></div>
			<div class="top-section__btns">
				<?php if(!empty($top_want)):?>
					<a href="#contacts" class="scroll-btn btn btn--bg">
						<span><?php echo $top_want; ?></span>
					</a>
				<?php endif;?>
				<?php if(!empty($top_cooperation)):?>
					<a href="#cooperation" class="scroll-btn btn btn--white">
						<span><?php echo $top_cooperation; ?></span>
					</a>
				<?php endif;?>
			</div>
		</div>
		<a href="#vacancy" class="down scroll-btn">
			<div class="svg"><svg width="16" height="8"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?php echo TX_URL_THEME; ?>/images/sprite.svg#down"></use></svg></div>
		</a>
	</div>
</div>
