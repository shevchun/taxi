<?php if(get_sub_field('show_about')):
	$about_image = get_sub_field('about_image')?get_sub_field('about_image'):'';
	$about_title = get_sub_field('about_title')?get_sub_field('about_title'):'';
?>
<div class="about viewport" id="about">
	<div class="container">
		<div class="about__box">
			<div class="about__img">
				<img src="<?php field_get_image('about_image','about-main',true)?>"
				     width="603" height="570" title="<?php echo $about_image["alt"]; ?>"
				     alt="<?php echo $about_image["title"]; ?>">
			</div>
			<div class="about__info">
				<div class="title"><?php echo $about_title; ?></div>
				<?php if(have_rows('triggers')):?>
					<div class="about__triggers">
						<?php while(have_rows('triggers')): the_row();?>
							<div class="about__trigger">
								<div class="about__trigger-img">
									<?php $icon = get_sub_field('icon')?get_sub_field('icon'):''; ?>
									<img src="<?php echo $icon["url"]; ?>" width="44" height="44"
									     alt="<?php echo $icon["title"]; ?>">
								</div>
								<div class="about__trigger-info">
									<div class="about__trigger-title"><?php echo get_sub_field('title');?></div>
									<div class="about__trigger-text">
										<?php echo get_sub_field('description');?>
									</div>
								</div>
							</div>
						<?php endwhile; ?>
					</div>
				<?php endif; ?>
			</div>
		</div>
	</div>
</div>
<?php endif; ?>