<?php if(get_sub_field('show_demands')):
	$demands_title = get_sub_field('demands_title')?get_sub_field('demands_title'):'';
	$demands_image = get_sub_field('demands_image')?get_sub_field('demands_image'):'';
?>
<div class="about about--cooperation viewport" id="demands">
	<div class="container">
		<div class="about__box">
			<div class="about__img">
				<img src="<?php field_get_image('demands_image','about-main',true)?>"
				     width="603" height="570" title="<?php echo $demands_image["alt"]; ?>"
				     alt="<?php echo $demands_image["title"]; ?>">
			</div>
			<div class="about__info">
				<div class="title"><?php echo $demands_title; ?></div>
	            <?php if(have_rows('demands')):?>
				<div class="about__triggers">
		            <?php while(have_rows('demands')): the_row();?>
						<div class="about__trigger">
							<?php $icon = get_sub_field('icon')?get_sub_field('icon'):'';?>
                            <?php if(!empty($icon)):?>
                                <div class="about__trigger-img">
                                    <img src="<?php echo $icon["url"];?>"
                                         width="40" height="40" alt="<?php echo $icon["title"];?>">
                                </div>
                            <?php endif; ?>
							<div class="about__trigger-info">
								<div class="about__trigger-title">
                                    <?php echo get_sub_field('text');?>
                                </div>
							</div>
						</div>
                    <?php endwhile; ?>
				</div>
                <?php endif; ?>
			</div>
		</div>
	</div>
</div>
<?php endif; ?>