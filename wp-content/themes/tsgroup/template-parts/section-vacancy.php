<?php if(get_sub_field('show_vacancy')):
	$vacancy_title = get_sub_field('vacancy_title')?get_sub_field('vacancy_title'):'';
	$button_text = get_sub_field('button_text')?get_sub_field('button_text'):'';
	?>
	<div class="vacancy viewport" id="vacancy">
		<div class="container">
			<div class="vacancy__box">
				<div class="title"><?php echo $vacancy_title; ?></div>
				<?php if(have_rows('vacancy')):?>
				<div class="vacancy__list">
					<?php while(have_rows('vacancy')): the_row();?>
					<div class="vacancy__item">
						<div class="vacancy__item-title"><?php echo get_sub_field('position');?></div>
						<div class="vacancy__item-text text-content scroll">
							<?php echo get_sub_field('description');?>
						</div>
                        <a href="#contacts" class="scroll-btn">
                            <div class="btn vacancy__btn"><span><?php echo $button_text; ?></span></div>
                        </a>
					</div>
					<?php endwhile; ?>
				</div>
				<?php endif; ?>
			</div>
		</div>
	</div>
<?php endif; ?>