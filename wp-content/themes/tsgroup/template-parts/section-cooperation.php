<?php if(get_sub_field('show_cooperation')):
	$cooperation_title = get_sub_field('cooperation_title')?get_sub_field('cooperation_title'):'';
	$button_text = get_sub_field('button_text')?get_sub_field('button_text'):'';
?>
<div class="cooperation viewport" id="cooperation">
	<div class="container-inner">
		<div class="title"><?php echo $cooperation_title; ?></div>
		<div class="cooperation__list-box">
			<?php if(have_rows('counters')):?>
				<div class="cooperation__list">
					<?php while(have_rows('counters')): the_row();?>
						<div class="cooperation__item">
							<div class="cooperation__item-inner">
								<div class="cooperation__value timer" data-from="0" data-to="<?php echo get_sub_field('value'); ?>">
                                    <?php echo get_sub_field('value'); ?>
                                </div>
								<div class="cooperation__text" >
                                    <?php echo get_sub_field('text'); ?>
                                </div>
							</div>
						</div>
					<?php endwhile; ?>
				</div>
			<?php endif; ?>
		</div>
        <a href="#contacts" class="scroll-btn">
            <div class="btn"><span><?php echo $button_text; ?></span></div>
        </a>
	</div>
</div>
<?php endif; ?>
