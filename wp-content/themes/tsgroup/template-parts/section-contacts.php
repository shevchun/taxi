<?php if(get_sub_field('show_contacts')):
	$contacts_title = get_sub_field('contacts_title')?get_sub_field('contacts_title'):'';
	$address = get_sub_field('address')?get_sub_field('address'):'';
	$pin = get_sub_field('pin')?get_sub_field('pin'):'';
	$phone_icon = get_sub_field('phone_icon')?get_sub_field('phone_icon'):'';
	$message_icon = get_sub_field('message_icon')?get_sub_field('message_icon'):'';
	$button_text = get_sub_field('button_text')?get_sub_field('button_text'):'';

	$map_coords = get_field('coords','option')?get_field('coords','option') : '';
?>
<div class="contacts viewport" id="contacts">
	<div class="container">
		<div class="contacts__box">
			<div class="contacts__left">
				<div class="title"><?php echo $contacts_title; ?></div>
				<div class="about__trigger">
					<?php if(!empty($address)):?>
						<div class="about__trigger-img">
							<img src="<?php echo $pin; ?>" width="37" height="38" alt="pin">
						</div>
						<div class="about__trigger-info">
							<div class="about__trigger-title"><address><?php echo $address;?></address></div>
						</div>
					<?php endif;?>
				</div>
				<?php if(have_rows('phone')):?>
					<div class="about__trigger">
							<?php if(!empty($phone_icon)):?>
							<div class="about__trigger-img">
								<img src="<?php echo $phone_icon?>" width="28" height="28" alt="phone_icon">
							</div>
							<?php endif; ?>
							<div class="about__trigger-info">
								<div class="about__trigger-title">
									<?php while(have_rows('phone')): the_row();?>
										<?php $phone = get_sub_field('phone')?get_sub_field('phone'):'';?>
										<a href="tel:<?php echo str_replace(array(' ','(',')','-'),'',$phone); ?>">
											<?php echo $phone; ?>
										</a>
									<?php endwhile; ?>
								</div>
							</div>
					</div>
				<?php endif; ?>
				<?php if(have_rows('mail')):?>
					<div class="about__trigger">
						<div class="about__trigger-img">
							<img src="<?php echo $message_icon; ?>" width="30" height="18" alt="letter">
						</div>
						<div class="about__trigger-info">
							<div class="about__trigger-title">
								<?php while(have_rows('mail')): the_row();?>
									<?php $mail = get_sub_field('mail')?get_sub_field('mail'):'';?>
									<a href="mailto:<?php echo $mail; ?>"><?php echo $mail; ?></a>
								<?php endwhile; ?>
							</div>
						</div>
					</div>
				<?php endif; ?>
				<div class="btn feedback"><span><?php echo $button_text;?></span></div>
			</div>
			<?php
			//Pars coords for map
			$map_array = array();
			if(!empty($map_coords)){
				$map_array = explode(',',$map_coords);
			}
			wp_localize_script('map-mail', 'mVars',
				array(
					'coords' => $map_array,
				)
			);
			?>
			<div class="map" id="map"></div>
		</div>
	</div>
</div>
<?php endif;?>
