<?php

get_header();
$error_description = get_field('error_description','option')?get_field('error_description','option') : '';
$error_image = get_field('error_image','option')?get_field('error_image','option') : '';
    ?>
    <div class="error-page">
        <div class="container">
            <div class="error-page__box">
                <div class="error-page__text">
                    <div class="error-page__caption">404</div>
                    <div class="error-page__descriptions"><?php echo $error_description; ?></div>
                    <div class="error-page__btn">
                        <a href="<?php echo home_url(); ?>" class="btn btn--bg"><span><?php _e('на главную','tsgroup');?></span></a>
                    </div>
                </div>
                <div class="error-page__img">
                    <img src="<?php echo $error_image["url"];?>"
                         width="603" height="495" alt="404">
                </div>
            </div>
        </div>
    </div>
<?php get_footer(); ?>
